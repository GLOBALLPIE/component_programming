// task7.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <map>
#include <unordered_set>


struct Account {
	unsigned int id; // �������������
	std::string login; // �����
	std::string name; // ���
	std::string shell; // ��������
	std::string home_directory; // �������� ����������
	bool operator==(const Account& acc) const {
		return id == acc.id && login == acc.login && name == acc.name &&
			shell == acc.shell && home_directory == acc.home_directory;
	}
};

struct hash_func {
	std::size_t operator()(const Account& acc) const {
		std::size_t id_hash = std::hash<unsigned int>()(acc.id);
		std::size_t login_hash = std::hash<std::string>()(acc.login);
		std::size_t name_hash = std::hash<std::string>()(acc.name);
		std::size_t shell_hash = std::hash<std::string>()(acc.shell);
		std::size_t directory_hash = std::hash<std::string>()(acc.home_directory);

		return id_hash ^ login_hash ^ name_hash ^ shell_hash ^ directory_hash;
	}
};

bool is_bash(Account& element, std::string&& expression) {
	return element.shell == expression;
}

struct compare {
	bool operator()(Account a, Account b) const {
		if (a.id != b.id) return a.id < b.id;
		if (a.login != b.login) return a.login < b.login;
		if (a.name != b.name) return a.name < b.name;
		if (a.shell != b.shell) return a.shell < b.shell;
		if (a.home_directory != b.home_directory) return a.home_directory < b.home_directory;
		return false;
	}
};

bool erase_func(const std::pair<Account, int> item) {
	auto const& [key, value] = item;
	return !(key.id >= 0 && key.id < 1000);
}

//struct erase_struct {
//	bool operator()(const std::pair<Account, int> item) {
//		auto const& [key, value] = item;
//		return (0 <= key.id && key.id < 1000);
//	}
//};

int main()
{
	std::vector<Account> database;
	database.push_back(Account{ 10, "5username", "5Name", "bin/bash", "home/5name/" });
	database.push_back(Account{ 12312312, "5username", "5Name", "bin/bash", "home/5name/" });
	database.push_back(Account{ 100, "5username", "5Name", "bin/bash", "home/5name/" });
	database.push_back(Account{ 200, "3username", "3Name", "bin/rash", "home/3name/" });
	database.push_back(Account{ 0, "username", "Name", "bin/hash", "home/name/" });
	//database.push_back(Account{ 1, "1username", "1Name", "bin/bash", "home/1name/" });
	database.push_back(Account{ 123214, "2username", "2Name", "bin/mash", "home/2name/" });
	database.push_back(Account{ 200, "3username", "3Name", "bin/rash", "home/3name/" });
	database.push_back(Account{ 123123, "4username", "81Name", "bin/dash", "home/4name/" });
	database.push_back(Account{ 123123, "4username", "4Name", "bin/dash", "home/4name/" });
	//database.push_back(Account{ 12, "5username", "5Name", "bin/bash", "home/5name/" });
	database.push_back(Account{ 1000, "5username", "7Name", "bin/bash", "home/5name/" });
	//database.push_back(Account{ 6, "6username", "6Name", "bin/zash", "home/6name/" });


	//1.
	/*using iterator = std::vector<Account>::iterator;
	iterator first = database.begin();
	iterator last = database.end();
	iterator current;
	auto equals_bash = std::bind(is_bash, std::placeholders::_1, "bin/bash");
	while (true) {
		first = std::ranges::find_if(first, last, equals_bash);
		if (first != last){
			std::cout << (*first).name << ' ';
			++first;
		}
		else
			break;
	}*/


	//2.
	//std::map<Account, int, compare> r;
	//const int n = database.size();
	//for (int i = 0; i < n; i++) {
	//	r[database[i]] = i;
	//}
	//for (auto const& kv : r)
	//	std::cout << kv.first.name << ' ' << kv.first.id << ' ' << kv.second << std::endl;

	//std::cout << std::endl;

	//const auto count = std::erase_if(r, erase_func);
	//for (auto const& kv : r)
	//	std::cout << kv.first.name <<' '<<kv.first.id<< ' ' << kv.second << std::endl;

	//3.
	//std::unordered_map<Account, int, hash_func> unord;
	//const int n = database.size();
	//for (int i = 0; i < n; i++) {
	//	unord[database[i]] = i;
	//}
	//std::unordered_map<std::string, int> count;
	///*for (auto const& kv : unord) {
	//	if (count.contains(kv.first.shell))
	//		count[kv.first.shell] += 1;
	//	else
	//		count[kv.first.shell] = 1;

	//}*/

	//for (int i = 0; i < n; i++) {
	//	if (count.contains(database[i].shell))
	//		count[database[i].shell] += 1;
	//	else
	//		count[database[i].shell] = 1;

	//}

	//for (auto const& kv : count)
	//	std::cout << kv.first << ' ' << kv.second << std::endl;

	//4.
	//std::unordered_set<Account, hash_func> unord_set(database.begin(), database.end());
	std::unordered_set<unsigned int> unord_set;

	const int n = database.size();
	for (int i = 0; i < n; i++) {
		unord_set.insert(database[i].id);
	}

	for (auto const& kv : database)
		if (unord_set.contains(kv.id))

			



}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
