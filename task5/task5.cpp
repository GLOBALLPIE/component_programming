﻿// task5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <string>
#include <iostream>
#include <sstream>
#include <array>
#include <tuple>
#include <vector>

// Код для message
void message(std::ostream& stream, const char* s) {
	while (*s) {
		if (*s == '%' && *++s != '%')
			throw std::runtime_error("invalid format string : missing arguments");
		stream << *s++;
	}
}


template<typename T, typename... Args>
void message(std::ostream& stream, const char* s, const T& value, const Args&... args) {
	while (*s) {
		if (*s == '%' && *++s != '%') {
			stream << value;
			return message(stream, s, args...);
		}
		stream << *s++;
	}
	throw std::runtime_error("extra arguments provided to print");
}

// Код для cat
template<typename T, std::size_t N, std::size_t M>
std::array<T, N + M> concat(std::array<T, N>& arg1, std::array<T, M>& arg2) {
	std::array<T, N + M> result;
	std::copy(arg1.cbegin(), arg1.cend(), result.begin());
	std::copy(arg2.cbegin(), arg2.cend(), result.begin() + N);
	return result;
}


template<typename T, std::size_t N, std::size_t M>
auto cat(std::array<T, N>& arg1, std::array<T, M>& arg2) {
	return concat(arg1, arg2);
}

template<typename T, std::size_t N, std::size_t M, typename... Args>
auto cat(std::array<T, N>& arg1, std::array<T, M>& arg2, Args&... args) {
	auto result = concat(arg1, arg2);
	return cat(result, args...);
}

// Код для tie


template<typename T, unsigned N, typename... REST>
struct generate_tuple_type
{
	typedef typename generate_tuple_type<T, N - 1, T, REST...>::type type;
};

template<typename T, typename... REST>
struct generate_tuple_type<T, 0, REST...>
{
	typedef std::tuple<REST...> type;
};

//template<class T, int N>
//struct container: container<T,N-1> {
//	template<class... Args>
//	container(T t, Args... args) : container<T, N - 1>(args...), head(t) {}
//};
//
//template<class T>
//struct container<T, 1> {
//	T head;
//	container(T t): head(t) {
//
//	}
//};

//template<class T>
//struct container<T,0>{};

//template <class T, int N, int M>
//struct Tie {
//	using refs_tuple = typename generate_tuple_type<std::array<T, N>&, M>::type;
//	refs_tuple refs;
//	void operator=(const std::array<T, N* M>& rhs) {
//		//set_value(rhs, M);
//		int a = 0;
//		for (int i = 0; i < M; i++)
//			std::copy(rhs.begin() + N * (i), rhs.begin() + N * (i + 1), std::get<0>(refs).begin());
//
//	}
//};

template<typename T, int N>
void push_back(std::vector< std::tuple< std::array<T, N>& > >& refs, std::array<T, N>& arg) {
	refs.push_back(std::tuple< std::array<T, N>& >(arg));
}

template<typename T, int N, typename... Args>
void push_back(std::vector< std::tuple< std::array<T, N>& > >& refs, std::array<T, N>& arg1, Args&... args) {
	refs.push_back(std::tuple< std::array<T, N>& >(arg1));
	push_back(refs, args...);
}


template <class T, int N, int M>
struct Tie {
	std::vector<std::tuple<std::array<T, N>&>> refs;

	void operator=(const std::array<T, N* M>& rhs) {
		for (int i = 0; i < M; i++)
			std::copy(rhs.begin() + N * i, rhs.begin() + N * (i + 1), std::get<0>(refs[i]).begin());

	}
};

template<typename T, int N, typename... Args>
auto my_tie(std::array<T,N>& arg1, Args&... args) {
	std::vector< std::tuple< std::array<T, N>& > > refs;
	push_back(refs, arg1, args...);
	return Tie<T, N, sizeof...(args) + 1>{ refs };
}

int main()
{

	//Код для функции message
	/*std::stringstream stream;

	message(stream, "%\n", 'a');
	message(stream, "%+%=%\n", 'a', 2, 3.0);
	message(stream, "% + % = %\n", 1, 2, 3);
	message(stream, "% + % = %\n", 1, 2, "asdafa");
	std::string s;
	std::string token;
	while (std::getline(stream, token, '\n')) {
		std::cout << token << '\n';
	}*/

	//Код для функции cat
	/*std::array<float, 3> vec1{ 1.0f,2.0f,3.0f };
	std::array<float, 3> vec2{ 4.0f,5.0f,6.0f };
	std::array<float, 3> vec3{ 7.0f,8.0f,9.0f };
	std::array<float, 3> vec4{ 10.0f,11.0f,12.0f };

	std::array<std::string, 3> vec1{ "a","b","c"};
	std::array<std::string, 3> vec2{ "d","e","f" };
	std::array<std::string, 3> vec3{ "g", "h", "i" };
	std::array<std::string, 3> vec4{ "j", "k", "l" };


	auto r = cat(vec1, vec2, vec3, vec4);
	for (auto x : r)
		std::cout << x << ' ';*/


		//std::array<std::string, 3> vec1{ "a","b","c" };
		//std::array<std::string, 3> vec2{ "d","e","f" };
		//std::array<std::string, 3> vec3{ "g", "h", "i" };
		//std::array<std::string, 3> vec4{ "j", "k", "l" };
		//tie(vec1, vec2);
		//std::cout << std::get<0>(tie(vec1, vec2))[0];

	std::array<float, 6> r{ 1.0f,2.0f,3.0f,4.0f,5.0f,6.0f };
	std::array<float, 3> vec1, vec2;

	std::vector< std::tuple< std::array<float, 3>& > > vec;
	my_tie(vec1, vec2) = r;
	//push_back(vec, vec1, vec2);

	/*Tie<float, 3, 2> t{ {std::tuple<std::array<float, 3>&>(vec1), std::tuple<std::array<float, 3>&>(vec2)} };
	t = r;*/

	//std::vector< std::tuple< std::array<float, 3>& > > vec;
	//vec.push_back(std::tuple<std::array<float, 3>&>(vec1));
	//vec.push_back(std::tuple<std::array<float, 3>&>(vec2));
	//std::get<0>(vec[0])[0].begin()
	//std::copy(r.begin(), r.begin() + 3, std::get<0>(vec[0]).begin());
	//std::copy(r.begin()+3, r.begin() + 6, std::get<0>(vec[1]).begin());
	/*for(int i=0;i<2;i++)
		std::copy(r.begin()+3*i, r.begin() + 3*(i+1), std::get<0>(vec[i]).begin());

	std::cout << vec1[0];*/

	//Tie<float, 3, 2> t{ {vec1, vec2} };

	//Tie<float, 3, 2> t{ {vec1, vec2} };
	//my_tie(vec1, vec2);
	for (auto arg : vec1)
		std::cout << arg << ' ';
	for (auto arg : vec2)
		std::cout << arg << ' ';


	//std::copy(r.begin(), r.begin() + 3, vec1.begin());
	//std::cout << vec1[0];

	//my_tie(vec1, vec2) = r; // (1 2 3) (4 5 6)


	//container<int, 1> a(1);

	//Tie<float, 3, 2> a(std::tuple<std::array<float,3>&, std::array<float, 3>&>(vec1, vec2));
	//Tie<float, 3, 2> e(vec1, vec2);
	//e.fill_references(vec1, vec2);
	//my_tie(vec1, vec2) = r; // (1 2 3) (4 5 6)
	//my_tie(vec1, vec2); // (1 2 3) (4 5 6)
	//using hand_tuple_t = std::tuple<int, int, int>;
	//static_assert(std::is_same<gen_tuple_t, hand_tuple_t>::value, "different types");


	//std::tuple<std::array<float,1>&> arr;
	//std::array<float, 1> a{ 1.0f }, b{2.0f};
	////arr[0] = a;
	////arr[1] = b;
	//arr
	//std::cout << a[0] << " " << b[0]<<"\n";
	//arr[0][0] = 20.0f;
	//std::cout << a[0] << " " << b[0]<<"\n";



	//Полезный пример
	/*using generate_tuple = generate_tuple_type<std::array<float, 3>&, 2>;
	using array_tuple = generate_tuple::type;
	array_tuple a(vec1, vec1);*/
	//a(vec1, vec2);
	//std::tuple<std::array<float, 3>, std::array<float, 3>> b({ 1,2,3 }, {4,5,6});
	//a = b;
	//std::cout << std::get<0>(a)[1];

	return 0;
}



template<typename... Args>
auto get_tuple(Args&... args) {
	return std::tuple<Args&...>(args);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
