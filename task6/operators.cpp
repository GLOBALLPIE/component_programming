// operators.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ostream>
#include <vector>


class Expression {};

template <class T>
class Vector;

template <class E>
Vector<typename E::value_type> evaluate(const E& expr) {
	using value_type = typename E::value_type;
	const auto n = expr.size();
	Vector<value_type> result(n);
	for (int i = 0; i < n; ++i) {
		result(i) = expr.evaluate(i);
	}
	return result;
}

template <class T>
class Vector : public Expression {

public:
	using value_type = T;

private:
	std::vector<T> _data;

public:

	Vector(std::initializer_list<T> rhs) : _data(rhs) {}
	explicit Vector(int n) : _data(n) {}
	template <class E>
	Vector(const E& expr,
		typename std::enable_if<std::is_base_of<Expression, E>::value, E>::type* dummy = nullptr) :
		Vector(::evaluate(expr)) {}

	Vector() = default;
	~Vector() = default;
	Vector(Vector&& rhs) = default;
	Vector(const Vector& rhs) = default;
	Vector& operator=(Vector&& rhs) = default;
	Vector& operator=(const Vector& rhs) = default;

	T& operator()(int i) { return this->_data[i]; }
	const T& operator()(int i) const { return this->_data[i]; }
	T evaluate(int i) { return this->_data[i]; }
	T evaluate(int i) const { return this->_data[i]; }
	int size() const { return this->_data.size(); }
	void display(std::ostream& out) const {
		out << "Vector(";
		const auto n = size();
		if (n != 0) { out << this->_data.front(); }
		for (int i = 1; i < n; ++i) { out << ',' << this->_data[i]; }
		out << ')';
	}

};

template <class E>
typename std::enable_if<std::is_base_of<Expression, E>::value, std::ostream&>::type
operator<<(std::ostream& out, const E& expr) {
	expr.display(out); return out;
}

template <class E1, class E2>
class Plus : public Expression {

public:
	using value_type =
		typename std::common_type<typename E1::value_type, typename E2::value_type>::type;

private:
	const E1& _a;
	const E2& _b;

public:
	explicit Plus(const E1& a, const E2& b) : _a(a), _b(b) {}
	value_type evaluate(int i) { return this->_a.evaluate(i) + this->_b.evaluate(i); }
	value_type evaluate(int i) const { return this->_a.evaluate(i) + this->_b.evaluate(i); }
	int size() const { return this->_a.size(); }
	void display(std::ostream& out) const {
		out << "Plus(" << this->_a << ", " << this->_b << ')';
	}

};

template <class E1, class E2>
typename std::enable_if<std::is_base_of<Expression, E1>::value&&
	std::is_base_of<Expression, E2>::value, Plus<E1, E2>>::type
	operator+(const E1& a, const E2& b) {
	return Plus<E1, E2>(a, b);
}

template<class E>
class UnaryMinus : public Expression {
public:
	//using value_type = typename E::value_type;
	using value_type = typename decltype(+E::value_type());

private:
	const E& val;
public:
	explicit UnaryMinus(const E& arg) : val(arg) {}
	value_type evaluate(int i) { return (-1) * this->val.evaluate(i); }
	value_type evaluate(int i) const { return (-1) * this->val.evaluate(i); }
	int size() const { return this->val.size(); }
	void display(std::ostream& out) const {
		out << "UnaryMinus(" << this->val << ')';
	}

};

template<class E>
typename std::enable_if<std::is_base_of<Expression, E>::value, UnaryMinus<E>>::type
operator-(const E& arg) {
	return UnaryMinus<E>(arg);
}

template<class E>
class UnaryPlus : public Expression {
public:
	using value_type = typename decltype(+E::value_type());
	//using value_type = typename decltype(std::declval<E::value_type>().operator+());
	//using value_type = typename E::value_type;
private:
	const E& val;
public:
	explicit UnaryPlus(const E& arg) : val(arg) {}
	value_type evaluate(int i) { return   +this->val.evaluate(i); }
	value_type evaluate(int i) const { return +this->val.evaluate(i); }
	int size() const { return this->val.size(); }
	void display(std::ostream& out) const {
		out << "UnaryPlus(" << this->val << ')';
	}


};


template<class E>
typename std::enable_if<std::is_base_of<Expression, E>::value, UnaryPlus<E>>::type
operator+(const E& arg) {
	return UnaryPlus<E>(arg);
}

template <class E1, class E2>
class Minus : public Expression {

public:
	using value_type =
		typename std::common_type<typename E1::value_type, typename E2::value_type>::type;

private:
	const E1& _a;
	const E2& _b;

public:
	explicit Minus(const E1& a, const E2& b) : _a(a), _b(b) {}
	value_type evaluate(int i) { return this->_a.evaluate(i) - this->_b.evaluate(i); }
	value_type evaluate(int i) const { return this->_a.evaluate(i) - this->_b.evaluate(i); }
	int size() const { return this->_a.size(); }
	void display(std::ostream& out) const {
		out << "Minus(" << this->_a << ", " << this->_b << ')';
	}
};

template <class E1, class E2>
typename std::enable_if<std::is_base_of<Expression, E1>::value&&
	std::is_base_of<Expression, E2>::value, Minus<E1, E2>>::type
	operator-(const E1& a, const E2& b) {
	return Minus<E1, E2>(a, b);
}

template <class E1, class E2>
class Multiply : public Expression {

public:
	using value_type =
		typename std::common_type<typename E1::value_type, typename E2::value_type>::type;

private:
	const E1& _a;
	const E2& _b;

public:
	explicit Multiply(const E1& a, const E2& b) : _a(a), _b(b) {}
	value_type evaluate(int i) { return this->_a.evaluate(i) * this->_b.evaluate(i); }
	value_type evaluate(int i) const { return this->_a.evaluate(i) * this->_b.evaluate(i); }
	int size() const { return this->_a.size(); }
	void display(std::ostream& out) const {
		out << "Multiply(" << this->_a << ", " << this->_b << ')';
	}
};

template <class E1, class E2>
typename std::enable_if<std::is_base_of<Expression, E1>::value&&
	std::is_base_of<Expression, E2>::value, Multiply<E1, E2>>::type
	operator*(const E1& a, const E2& b) {
	return Multiply<E1, E2>(a, b);
}

template <class E1, class E2>
class Divide : public Expression {

public:
	using value_type =
		typename std::common_type<typename E1::value_type, typename E2::value_type>::type;

private:
	const E1& _a;
	const E2& _b;

public:
	explicit Divide(const E1& a, const E2& b) : _a(a), _b(b) {}
	value_type evaluate(int i) { return this->_a.evaluate(i) / this->_b.evaluate(i); }
	value_type evaluate(int i) const { return this->_a.evaluate(i) / this->_b.evaluate(i); }
	int size() const { return this->_a.size(); }
	void display(std::ostream& out) const {
		out << "Divide(" << this->_a << ", " << this->_b << ')';
	}
};

template <class E1, class E2>
typename std::enable_if<std::is_base_of<Expression, E1>::value&&
	std::is_base_of<Expression, E2>::value, Divide<E1, E2>>::type
	operator/(const E1& a, const E2& b) {
	return Divide<E1, E2>(a, b);
}

template <class E1, class E2>
class Less : public Expression {

	//public:
	//	using value_type = bool;
public:
	using value_type =
		typename std::common_type<typename E1::value_type, typename E2::value_type>::type;

private:
	const E1& _a;
	const E2& _b;

public:
	explicit Less(const E1& a, const E2& b) : _a(a), _b(b) {}
	bool evaluate(int i) { return this->_a.evaluate(i) < this->_b.evaluate(i); }
	bool evaluate(int i) const { return this->_a.evaluate(i) < this->_b.evaluate(i); }
	int size() const { return this->_a.size(); }
	void display(std::ostream& out) const {
		out << "Less(" << this->_a << ", " << this->_b << ')';
	}
};

template <class E1, class E2>
typename std::enable_if<std::is_base_of<Expression, E1>::value&&
	std::is_base_of<Expression, E2>::value, Less<E1, E2>>::type
	operator<(const E1& a, const E2& b) {
	return Less<E1, E2>(a, b);
}

template<class E>
class All : public Expression {
public:
	using value_type = typename E::value_type;
private:
	const E& val;

public:
	explicit All(const E& arg) : val(arg) {}
	bool evaluate(int i) { return this->val.evaluate(i) != 0; }
	bool evaluate(int i) const { return this->val.evaluate(i) != 0; }
	int size() const { return this->val.size(); }
	void display(std::ostream& out) const {
		out << "All(" << this->val << ')';
	}
};

template <class E>
typename std::enable_if<std::is_base_of<Expression, E>::value, bool>::type
all(const E& val) {
	All<E> expr(val);
	const auto n = expr.size();
	int result;
	result = 1;
	for (int i = 0; i < n; i++) {
		result *= expr.evaluate(i);
	}
	return result;
}

int main(int argc, char* argv[]) {

	using T = float;
	Vector<T> a{ 1,2,3 }, b{ 4,5,6 }, c{ 7,8, 9 }, d{ 0,0,4 };
	//Vector<bool> vec{ 1,0,1 };
	//Vector<bool> f{ 1,1,1 };
	Vector<char> h{ 'a', 'b', 'c' };
	//Vector<int> g = +f;
	//Vector<std::string> k{ "a", "b", "c" };
	//std::cout << h << std::endl;
	//std::cout << evaluate(+h) << std::endl;
	//std::cout << h - b + c << std::endl;
	//std::cout << evaluate(h - b + c) << std::endl;
	//std::cout << evaluate(b * c) << std::endl;
	//std::cout << evaluate(h * c) << std::endl;
	//std::cout << h * h << std::endl;
	//std::cout << evaluate(f * h) << std::endl;
	std::cout << a << std::endl;
	std::cout << (a * h / c + d) * a << std::endl;
	std::cout << evaluate((a * h / c + d) * a) << std::endl;
	std::cout << (a < (a* h / c + d)* a) << std::endl;
	std::cout << evaluate(a < (a* h / c + d)* a) << std::endl;
	std::cout << all(a < (a* h / c + d)* a) << std::endl;

	//Vector<bool> vb;
	//vb = { true };
	//vb = { };
	//std::cout << vb;

	//using val_type = decltype(vb(0));
	//std::vector<val_type> refs;
	//std::cout << vb(0);
	/*for (int i = 0; i < vb.size(); i++) {
		p = vb.evaluate(i);
		p = 5 > 3;
		std::cout << vb(i) << std::endl;
	}*/

	/*std::vector<bool> vb(6);
	std::vector<std::vector<bool>::reference> refs;
	for (int i = 0; i < vb.size(); i++) {
		refs.push_back(vb[i]);
		refs[i] = 5 > 3;
		std::cout << vb[i] << std::endl;
	}*/
	return 0;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
