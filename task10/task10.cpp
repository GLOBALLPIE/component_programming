﻿#include <iostream>
#include <thread>

template<typename T>
void MergeSort(T a[], size_t l)
{
	size_t BlockSizeIterator;
	size_t BlockIterator;
	size_t LeftBlockIterator;
	size_t RightBlockIterator;
	size_t MergeIterator;

	size_t LeftBorder;
	size_t MidBorder;
	size_t RightBorder;
	for (BlockSizeIterator = 1; BlockSizeIterator < l; BlockSizeIterator *= 2)
	{
		for (BlockIterator = 0; BlockIterator < l - BlockSizeIterator; BlockIterator += 2 * BlockSizeIterator)
		{
			//Производим слияние с сортировкой пары блоков начинающуюся с элемента BlockIterator
			//левый размером BlockSizeIterator, правый размером BlockSizeIterator или меньше
			LeftBlockIterator = 0;
			RightBlockIterator = 0;
			LeftBorder = BlockIterator;
			MidBorder = BlockIterator + BlockSizeIterator;
			RightBorder = BlockIterator + 2 * BlockSizeIterator;
			RightBorder = (RightBorder < l) ? RightBorder : l;
			int* SortedBlock = new int[RightBorder - LeftBorder];

			//Пока в обоих массивах есть элементы выбираем меньший из них и заносим в отсортированный блок
			while (LeftBorder + LeftBlockIterator < MidBorder && MidBorder + RightBlockIterator < RightBorder)
			{
				if (a[LeftBorder + LeftBlockIterator] < a[MidBorder + RightBlockIterator])
				{
					SortedBlock[LeftBlockIterator + RightBlockIterator] = a[LeftBorder + LeftBlockIterator];
					LeftBlockIterator += 1;
				}
				else
				{
					SortedBlock[LeftBlockIterator + RightBlockIterator] = a[MidBorder + RightBlockIterator];
					RightBlockIterator += 1;
				}
			}
			//После этого заносим оставшиеся элементы из левого или правого блока
			while (LeftBorder + LeftBlockIterator < MidBorder)
			{
				SortedBlock[LeftBlockIterator + RightBlockIterator] = a[LeftBorder + LeftBlockIterator];
				LeftBlockIterator += 1;
			}
			while (MidBorder + RightBlockIterator < RightBorder)
			{
				SortedBlock[LeftBlockIterator + RightBlockIterator] = a[MidBorder + RightBlockIterator];
				RightBlockIterator += 1;
			}

			for (MergeIterator = 0; MergeIterator < LeftBlockIterator + RightBlockIterator; MergeIterator++)
			{
				a[LeftBorder + MergeIterator] = SortedBlock[MergeIterator];
			}
			delete SortedBlock;
		}
	}
}

template<typename T>
void merge(T sequence[], int size)
{
	T* sorted = new T[size];
	int middle = size / 2;

	int index_left = 0;
	int index_right = middle;
	int index_sequence = 0;

	while (index_left < middle && index_right < size)
	{
		if (sequence[index_left] < sequence[index_right])
			sorted[index_sequence++] = sequence[index_left++];
		else
			sorted[index_sequence++] = sequence[index_right++];
	}

	while (index_left < middle)
		sorted[index_sequence++] = sequence[index_left++];

	while (index_right < size)
		sorted[index_sequence++] = sequence[index_right++];

	for (int i = 0; i < size; i++)
		sequence[i] = sorted[i];

	delete[] sorted;
}

template<typename T>
void merge_sort(T sequence[], int size, int threads_number = 0)
{
	if (threads_number >= std::thread::hardware_concurrency()) {
		std::cout << "Max number of threads" << std::endl;
		std::cout << std::this_thread::get_id() << ' ' << threads_number << std::endl;
		MergeSort(sequence, size);
	}
	else {
		if (size > 1)
		{
			std::cout << std::this_thread::get_id() << ' ' << threads_number << std::endl;
			int middle = size / 2;
			std::thread left(merge_sort<T>, &sequence[0], middle, threads_number + 2);
			std::thread right(merge_sort<T>, &sequence[middle], size - middle, threads_number + 2);
			left.join();
			right.join();

			merge<T>(sequence, size);
		}

	}


}


//template <class T>
//void merge_sort(T arr[], int begin, int end, int threads_number = 0) {
//    if (threads_number >= std::thread::hardware_concurrency())
//        MergeSort(arr + begin, end);
//    else {
//        const int n = end - begin;
//
//    }
//}

int main()
{
	const int n = 12 * 3;
	int arr[n] = { 0, -1, -2, 3, 4, 21, -2, -3123, 2, 3,4, 5, 0, -1, -2, 3, 4, 21, -2, -3123, 2, 3,4, 5, 0, -1, -2, 3, 4, 21, -2, -3123, 2, 3,4, 5 };

	//MergeSort(arr, n);
	merge_sort(arr, n);

	for (int i = 0; i < n; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
	return 0;
}