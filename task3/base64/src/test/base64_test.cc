#include <../base64/base64.hh>
#include <gtest/gtest.h>
#include <string>

struct base64_param{
	std::string input;
	std::string output;
};

class base64_test1: public ::testing::TestWithParam<base64_param>{};

class base64_test2: public ::testing::TestWithParam<base64_param>{};

TEST(base64_test0, base64_encoded_size_exception){
	EXPECT_THROW(base64_encoded_size(std::numeric_limits<size_t>::max()), std::length_error);
}
TEST(base64_test0, char_to_index_exceptions){
	EXPECT_THROW(char_to_index('\x81'), std::invalid_argument);
	EXPECT_THROW(char_to_index('\x0c'), std::invalid_argument);
}

TEST(base64_test0, decode_exception){
	std::string s = "Zm9";
	//std::string s = "Zm9v";
	//std::string expected = "foo";

	std::string result;
	result.resize(base64_max_decoded_size(s.size()));
	EXPECT_THROW(base64_decode(s.data(), s.size(), &result[0]), std::invalid_argument);
}

TEST_P(base64_test1, encode){

const base64_param& param = GetParam();

	std::string s = param.input;
	std::string expected = param.output;

	std::string result;
	result.resize(base64_encoded_size(s.size()));
	base64_encode(s.data(),s.size(),&result[0]);
	EXPECT_EQ(expected, result);

}

TEST_P(base64_test2, decode){
	const base64_param& param = GetParam();

	std::string s = param.input;
	std::string expected = param.output;

	std::string result;
	result.resize(base64_max_decoded_size(s.size()));
	auto actual_size = base64_decode(s.data(), s.size(), &result[0]);
	result.resize(actual_size);
	EXPECT_EQ(expected, result);

}
//INSTANTIATE_TEST_CASE_P
INSTANTIATE_TEST_CASE_P(param_test, base64_test1, ::testing::Values(
	base64_param{"Man is distinguished, not only by his reason, but by this singular passion from other "
"animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable"
" generation of knowledge, exceeds the short vehemence of any carnal pleasure.",
"TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0"
"aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1"
"c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0"
"aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdl"
"LCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="
	},
	base64_param{"foob","Zm9vYg=="},
	base64_param{"", ""}
  )
);

INSTANTIATE_TEST_CASE_P(param_test, base64_test2, ::testing::Values(
	base64_param{"TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0"
"aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1"
"c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0"
"aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdl"
"LCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=",
"Man is distinguished, not only by his reason, but by this singular passion from"
" other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable"
" generation of knowledge, exceeds the short vehemence of any carnal pleasure."
	},
	base64_param{"Zm9vYg==", "foob"},
	base64_param{"",""},
	base64_param{"Zg==", "f"},
	base64_param{"Zm9vYmFy", "foobar"},
	base64_param{"Zm=Z", "f`\x19"}
  )
);

/*TEST(base64, encode){
	std::string s = "Man is distinguished, not only by his reason, but by this singular passion from other "
"animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable"
" generation of knowledge, exceeds the short vehemence of any carnal pleasure.";
	std::string output = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0"
"aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1"
"c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0"
"aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdl"
"LCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";
	std::string result;

	result.resize(base64_encoded_size(s.size()));
	base64_encode(s.data(),s.size(),&result[0]);
	EXPECT_EQ(output, result);
}

TEST(base64, decode){
	std::string s = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0"
"aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1"
"c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0"
"aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdl"
"LCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";
	std::string expected = "Man is distinguished, not only by his reason, but by this singular passion from"
" other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable"
" generation of knowledge, exceeds the short vehemence of any carnal pleasure.";

	std::string result;
	result.resize(base64_max_decoded_size(s.size()));
	auto actual_size = base64_decode(s.data(), s.size(), &result[0]);
	result.resize(actual_size);
	EXPECT_EQ(expected, result);
}
*/
