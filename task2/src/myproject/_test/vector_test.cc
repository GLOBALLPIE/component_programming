#include <gtest/gtest.h>
#include <myproject/vector.hh>
#include <string>
#include <fstream>
#include <typeinfo>

template <class T>
struct erase_params {
// исходные элементы
std::vector<T> elements;
// начальный индекс, с которого начинается удаление элементов
size_t start_index;
// конечный индекс, на котором заканчивается удаление элементов
size_t end_index;
// элементы, которые останутся в векторе после удаления
std::vector<T> result;
};

class int_erase_test : public ::testing::TestWithParam<erase_params<int>> {};

template<class T>
struct push_back_test: public ::testing::Test {};

TEST_P(int_erase_test, int_test) {
    const auto& params = GetParam();
    std::vector<int> x(params.elements);
    std::vector<int> y(params.result);
    x.erase(x.begin() + params.start_index, x.begin() + params.end_index);
    bool result = std::equal(x.begin(),x.end(),y.begin());
    EXPECT_TRUE(result);
 }


INSTANTIATE_TEST_CASE_P(INT_TEST, int_erase_test, ::testing::Values(
    erase_params<int>{ {0, 1, 2, 3, 4, 5}, 0, 2, {2,3,4,5} }
     )
    );

using types = ::testing::Types<float,std::ofstream, std::string>;

TYPED_TEST_CASE(push_back_test, types);

TYPED_TEST(push_back_test,with_types){
	std::vector<TypeParam> x;
	std::vector<TypeParam> y;
	x.push_back(TypeParam());
	y.push_back(TypeParam());
	x.pop_back();
	y.pop_back();
	EXPECT_EQ(x.size(),y.size());
}

TEST(vector, size){
	std::vector <float> x;
	EXPECT_EQ(0u, x.size());
	x.push_back(1);
	EXPECT_EQ(1u, x.size());
}

TEST(vector, push_back){
	std::vector <int> x;
	x.push_back(0);
	x.push_back(1);
	x.push_back(2);
	EXPECT_EQ(0u,x[0]);
	EXPECT_EQ(1u,x[1]);
	EXPECT_EQ(2u,x[2]);


}
TEST(vector, pop_back){
	std::vector <int> x;
	x.push_back(0);
	x.push_back(1);
	x.push_back(2);

	x.pop_back();
	x.pop_back();
	x.pop_back();
	EXPECT_EQ(0u,x.size());

}

TEST(vector, begin){
	std::vector<int> x;
	x.push_back(1);
	x.push_back(2);
	x.push_back(3);
	EXPECT_EQ(1u ,*(x.begin()));
}

TEST(vector, end){
	std::vector<int> x;
	x.push_back(1);
	x.push_back(2);
	x.push_back(3);
	auto iter = --x.end();
	EXPECT_EQ(3u, *(iter));

}
TEST(vector, equals){
	std::vector<int> x;
	std::vector<int> y{1,2,3};
	x=y;
	EXPECT_EQ(x,y);

}



std::vector<int> get_vector(){
	std::vector<int> result;
	result.push_back(0);
	result.push_back(1);
	result.push_back(2);
	return result;
}

TEST(vector, move_equals){
	std::vector<int> x;
	x=get_vector();
	EXPECT_EQ(0,*(x.begin()));
	EXPECT_EQ(1,*(x.begin()+1));
	EXPECT_EQ(2,*(x.begin()+2));
}

TEST(vector, constructor){
	std::vector<int> y{1,2,3};
	std::vector<int> x(y);
	EXPECT_EQ(x,y);
}
TEST(vector, move_constructor){
	std::vector<int> x(get_vector());
	EXPECT_EQ(0,*(x.begin()));
	EXPECT_EQ(1,*(x.begin()+1));
	EXPECT_EQ(2,*(x.begin()+2));

}
